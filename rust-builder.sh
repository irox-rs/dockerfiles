#!/bin/bash

docker build -f rust-builder \
	-t gitlab.com/irox-rs/gitlab-builder:latest \
	-t gitlab.com/irox-rs/gitlab-builder:`git describe` \
	empty
